package com.example.thomas.tomsapp;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Dialog;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MapActivity extends FragmentActivity implements LocationListener {
    // Initializing variables
    private TextView latitudeField;
    private TextView longitudeField;
    private LocationManager locationManager;
    private String provider;
    //Accessing our support map
    /*  method doesn't seem to work yet with supportfragments...
    GoogleMap supportMap = ((SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map)).getMap(); */
    // other method...
    /* FragmentManager fManager = getSupportFragmentManager();
    SupportMapFragment iMapFragment
            = (SupportMapFragment)fManager.findFragmentById(R.id.map);
    GoogleMap supportMap = iMapFragment.getMap(); */
    GoogleMap supportMap;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        //Position text fields
        latitudeField = (TextView) findViewById(R.id.textView);
        longitudeField = (TextView) findViewById(R.id.textView3);

        // initializing google play services status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if(status!= ConnectionResult.SUCCESS) { // if it's not available, print out the error for debugging
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
        else{
            { // if the google play services IS available

                // Get the map from the activity_map.xml fragment
                SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

                // creating a variable of the map
                supportMap = fm.getMap();

                // setting a layer of our location to the map
                supportMap.setMyLocationEnabled(true);

                // creating a location manager variable from location service
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                // Setting criteria for provider
                Criteria criteria = new Criteria();

                // Getting the name of the best provider
                String provider = locationManager.getBestProvider(criteria, true);

                // Getting Current Location
                Location location = locationManager.getLastKnownLocation(provider);

                //If we have a location
                if(location!=null){
                    onLocationChanged(location);
                }
                //If not, request location
                latitudeField.setText("Location not available");
                longitudeField.setText("Location not available");
                locationManager.requestLocationUpdates(provider, 20000, 0, this);
            }
        }

    }

    /* Request updates at startup
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    /* Remove the locationlistener updates when Activity is paused
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }*/

    @Override
    public void onLocationChanged(Location location) {
        double lat =  location.getLatitude();
        double lng =  location.getLongitude();

        latitudeField.setText(String.valueOf(lat));
        longitudeField.setText(String.valueOf(lng));

        LatLng currentLoc = new LatLng(lat, lng);

        // Showing the current location in Google Map
        supportMap.moveCamera(CameraUpdateFactory.newLatLng(currentLoc));

        // Zoom in the Google Map
        supportMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        /*
        supportMap.setMyLocationEnabled(true);
        supportMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLoc, 13));

        supportMap.addMarker(new MarkerOptions()
                .title("You are: ")
                .position(currentLoc));*/

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }
}
