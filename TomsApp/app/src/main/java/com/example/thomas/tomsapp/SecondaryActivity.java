package com.example.thomas.tomsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondaryActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        //Initializes different variables
        TextView txtName = (TextView) findViewById(R.id.txtName);
        Button mapButton = (Button) findViewById(R.id.gpsButton);
        Button closeButton = (Button) findViewById(R.id.btnClose);

        //Get intent
        Intent i = getIntent();
        //Gather data sent from the other activity
        String userName = i.getStringExtra("name");
        //Prints out to console
        Log.e("Secondary activity", userName);

        // Greets the user
        txtName.setText(userName);

        //Whenever the user clicks the "map" button, they'll get sent to a different activity with a map
        mapButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Sends user to the google map in another activity

                Intent iMap = new Intent(getApplicationContext(), MapActivity.class);

                startActivity(iMap);
            }
        });
        //Closes the activity and sends the user back to main
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0){

                finish();

            }
        });


    }
}