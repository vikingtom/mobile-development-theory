package com.example.thomas.tomsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.thomas.tomsapp.users;
import com.example.thomas.tomsapp.MySQLiteHelper;

import org.w3c.dom.UserDataHandler;

public class MainActivity extends Activity {
    // Initializing variables
    EditText inputName;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputName = (EditText) findViewById(R.id.name);
        Button btnNextScreen = (Button) findViewById(R.id.btnNextScreen);

        //Add a user
        final users thomas = (new users("Thomas motherfucking", 60.45, 10.89));

        //reference db
        MySQLiteHelper db = new MySQLiteHelper(this);
        thomas.addUser(db);
        //^This is simply a hack for debugging

        //Listening to button event
        btnNextScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), SecondaryActivity.class);

                //Sending data to another Activity
                nextScreen.putExtra("name", inputName.getText().toString());

                //Prints it to the console/logcat
                Log.e("n", inputName.getText()+".");

                startActivity(nextScreen);

            }
        });
    }
}
