package com.example.thomas.tomsapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.example.thomas.tomsapp.MySQLiteHelper;

/**
 * Created by Thomas on 15.09.2014.
 */
public class users {
    private int id;
    private String user;
    private double uLat;
    private double uLong;

    public users(){

    }

    public users(String user, Double uLat, Double uLong) {
        super();
        this.user = user;
        this.uLat = uLat;
        this.uLong = uLong;
    }



    @Override
    public String toString() {
        return "User [id=" + id + ", Latitude=" + uLat + ", Longitude=" + uLong
                + "]";
    }

    //table name
    private final static String TABLE_USERS = "users";
    //table columns
     final static String KEY_ID = "id";
     final static String KEY_USER = "user";
     final static String KEY_LAT = "lat";
     final static String KEY_LONG = "long";

    //Inserting columns into an array
    private final String[] COLUMNS = {KEY_ID,KEY_USER,KEY_LAT,KEY_LONG};

    public void addUser(MySQLiteHelper dbhelper){
        //for logging
        Log.d("adding User ", user);

        // db reference
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        users user = new users();

        // get values to add
        ContentValues values = new ContentValues();
        values.put(KEY_USER, user.getUser()); // get user
        values.put(KEY_LAT, user.getLat()); // get latitude
        values.put(KEY_LONG, user.getLong()); //get longitude

        // insert into db
        db.insert(TABLE_USERS, // table
                null,
                values);

        // 4. close
        db.close();
    }
    public users getUser(MySQLiteHelper dbHelper){

        // get reference
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // start query
        Cursor cursor =
                db.query(TABLE_USERS, // table
                        COLUMNS, // columns
                        " id = ?", // select
                        new String[] { String.valueOf(id) }, // arguments
                        null, //  group by
                        null, //  having
                        null, //  order by
                        null); //  limit

        //  get first if no result
        if (cursor != null)
            cursor.moveToFirst();

        // create user object
        users user = new users();
        user.setId(Integer.parseInt(cursor.getString(0)));
        user.setUser(cursor.getString(1));
        user.setLat(Integer.parseInt(cursor.getString(2)));
        user.setLong(Integer.parseInt(cursor.getString(3)));

        Log.d("getUser("+id+")", user.toString());

        // return user
        return user;
    }
    public users fetchAllUsers (users user){

        //SQl commands for fetching data within sqldb
        // SELECT * FROM users 



        return user;
    }
    public String getUser() {
        return user;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String setUser() {
        return user;
    }
    public double getLat() {
        return uLat;
    }
    public double getLong() {
        return uLong;
    }

    public void setLat(double uLat) {
        this.uLat = uLat;
    }
    public void setLong(double uLong) {
        this.uLong = uLong;
    }
    public void setUser (String user) {
        this.user =user;
    }


}
